/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package java.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import java.MvvmApplication
import java.dagger.modules.ContributeFragmentModule
import java.dagger.modules.ContributeViewModelModule

@Component(
    modules = [
        AndroidInjectionModule::class,
        ContributeFragmentModule::class,
        ContributeViewModelModule::class
    ]
)
interface MVVMComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): MVVMComponent
    }

    fun inject(app: MvvmApplication)
}